package com.example.bookshelf.storage.impl;

import com.example.bookshelf.Book;
import com.example.bookshelf.storage.BookStorage;

import java.util.ArrayList;
import java.util.List;

public class StaticListBookStorageImpl implements BookStorage {

    private static List<Book> bookStorage = new ArrayList<Book>();

    public StaticListBookStorageImpl() {
        bookStorage.add(new Book(10,"Harry Potter","J.K",500,2002,"publishing company ###"));

    }

    //     bookStorage.add(new Book(10,"Harry Potter","J.K",500,2002,"publishing company ###"))



    public Book getBook(long id) {
        for (Book book : bookStorage) {
            if (book.getId() == id) {
                return book;
            }
        }
        return null;
    }

//    public void Book removeBook(long id) {
//        for (Book book : bookStorage) {
//            if (book.getId()== id) {
//                ;
//            }
//        }
//
//    }
    public List<Book> getAllBooks() {
        System.out.println();
        return bookStorage;
    }

    public void addBook(Book book){
        bookStorage.add(book);
    }
}