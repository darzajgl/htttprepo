package com.school.jackson.json;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class Driver {

    public static void main(String[] args) {


        ObjectMapper mapper = new ObjectMapper();


        try {
            Student student = mapper.readValue(new File("data/sample-lite.json"),Student.class);


            System.out.println("First name: "+student.getFirstName());
            System.out.println("Last name: "+student.getLastName());



        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Student student = mapper.readValue(new File("data/sample-full.json"),Student.class);

            System.out.println("languages"+ Arrays.toString(student.getLanguages()));
            System.out.println("Address: "+ student.getAddress());




        } catch (IOException e) {
            e.printStackTrace();
        }

    }





}
